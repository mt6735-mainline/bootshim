# SPDX-License-Identifier: MIT

import os

env = Environment(
	tools = ["clang", "link"],
	LINKFLAGS=[
		"--target=arm-none-eabi",
		"-mcpu=cortex-a53",
		"-nostdlib",
		"-DAARCH64_BASE=$aarch64_base",
		"-Wl,--oformat=binary"
	],
	# TODO: Add command-line arguments to allow for customizing these values:
	aarch32_base = 0x40004000,
	aarch64_base = 0x40008000,
)

def append_padding(target, source, env):
	size = os.path.getsize(source[0].name)
	padding = env["aarch64_base"] - env["aarch32_base"] - size
	if padding < 0:
		raise Exception("distance between aarch32 and aarch64 bases too small")

	source_file = open(source[0].name, "rb")
	target_file = open(target[0].name, "wb")
	target_data = bytearray(source_file.read())

	for byte in range(padding):
		target_data.append(0)

	target_file.write(target_data)

padding = env.Builder(action = append_padding)
env.Append(BUILDERS = {"Padding": padding})

env.Program(
	source = "main.S",
	target = "bootshim.bin"
)

# Append zeroes to the end of the assembled boot shim to make its end reach the
# aarch64_base specified. This makes it possible to append a program to the
# resulting boot shim, forming a payload that can be loaded by the stock
# bootloader as a kernel image. This wil make the appended program land in
# memory at the aarch64_base specifed once loaded by the bootloader.
#
# 0x00000000
# ...
# 0x40000000	Memory base
# ...
# aarch32_base	Stock bootloader loads aarch32 kernel here. Boot shim will land
#				here once the payload is loaded by the bootloader as a kernel.
# ...			Padding to offset the program from the end of the boot shim.
# aarch64_base	Appended program will land here and get executed by the CPU once
#				switched to aarch64.
#
env.Padding(
	source = "bootshim.bin",
	target = "bootshim"
)
