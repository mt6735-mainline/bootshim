# MT6735 AArch64 boot shim
Some devices with the MediaTek MT6735 SoC or one of its variants ship with bootloaders that lack a certain mechanism to load and boot AArch64 (64-bit) kernels. These devices ship with AArch32 software. This allows for running AArch64 software by adding back the mode switch mechanism missing from the stock bootloader in the form of a tiny "shim" that runs between the bootloader and kernel stages, hence the name "boot shim".

## The mode switch mechanism
The secure monitor (TrustZone) implementation on MT6735 includes code to handle switching the boot CPU to AArch64. In order to make use of it, a secure monitor call is made by the bootloader, with an address passed as an argument from which the CPU will start executing after being switched. On non-broken stock bootloaders, the mode needed for the kernel is stored in a kernel command-line argument in the Android boot image:
```
bootopt=64S3,32N2,64N2
```
The argument consists of three elements, one for each of the secure monitor, bootloader and kernel, respectively. Each element consists of a processor mode (AArch*32*/AArch*64*), a world (*S*ecure/*N*ormal) and an exception level. The preloader reads the first two to determine how to run the secure monitor and bootloader, then once loaded, the bootloader reads the last one to decide whether a mode switch is necessary for the kernel. On broken bootloaders, nothing happens if the last element is changed, and the kernel always runs in AArch32.

## Devices known to have this issue:
- Samsung Galaxy Grand Prime+ (MT6737T)

## Building
SCons and Clang are needed to build. Run `scons` to build.

## Usage
A raw kernel image (or any AArch64 program that can run on bare metal for that matter) is appended to the resulting `bootshim` file, which contains the shim and padding to offset the kernel image from it. The image obtained from this can then be packed into an Android boot image as a kernel image then flashed normally. Once loaded by the stock bootloader, the boot shim will land in memory where the kernel is supposed to be, and the kernel image will land in another address which the boot shim will pass onto the secure monitor for the CPU to start execution at once switched to AArch64.

Due to the broken bootloader expecting a compressed self-decompressing `zImage`, it cannot decompress a kernel image on its own. A decompressed arm64 Linux `Image` will most likely be too big to fit in the `boot` partition, In which case a secondary bootloader must be loaded instead of a kernel image to handle loading (and possibly decompressing) a kernel image from somewhere else in storage.
